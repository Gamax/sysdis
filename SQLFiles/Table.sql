DROP TAbLE Article ;
DROP TAbLE Logs ;
DROP TAbLE Lot ;
DROP TAbLE Acheteur ;
DROP TAbLE vendeur ;

Create table Vendeur(Id int PRIMARY KEY AUTO_INCREMENT, 
	Nom varchar(30), 
	Prenom varchar(30), 
	Login varchar(30)
);

CREATE TABLE Lot(Id int PRIMARY KEY AUTO_INCREMENT, 
	Descriptif TEXT, 
	RefVendeur int, 
	DateAchat date, 
	PrixAchat DECIMAL(6,2),
	CONSTRAINT FK_Lot_Vendeur FOREIGN KEY(RefVendeur) REFERENCES Vendeur(Id)
);

CREATE TABLE Acheteur(
	Id int PRIMARY KEY AUTO_INCREMENT,  
	Nom varchar(30), 
	Prenom varchar(30), 
	Login varchar(30)
);

CREATE TABLE Article(
	Id int PRIMARY KEY AUTO_INCREMENT,  
	RefLot int, 
	Description TEXT, 
	PrixAnnonce DECIMAL(6,2), 
	PrixVente DECIMAL(6,2), 
	DateVente date,
	RefAcheteur int,
	PhotoPath TEXT,
	Photo blob,
	CONSTRAINT FK_Article_Acheteur FOREIGN KEY(RefAcheteur) REFERENCES Acheteur(Id),
	CONSTRAINT FK_Article_Lot FOREIGN KEY(RefLot) REFERENCES Lot(Id)
);

CREATE TABLE Logs(
	Id int PRIMARY KEY AUTO_INCREMENT,  
	Infos TEXT
);


INSERT INTO Vendeur (Nom,Prenom,login) VALUES("B�melmans","St�phanie","steph");
INSERT INTO Vendeur (Nom,Prenom,login) VALUES("Schutz","Dylan","dylan");
INSERT INTO Acheteur (Nom,Prenom,login) VALUES("Dethier","Thomas","thomas");
INSERT INTO Acheteur (Nom,Prenom,login) VALUES("Schyns","No�","noe");

commit;