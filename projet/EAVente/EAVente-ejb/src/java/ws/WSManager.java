/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ws;

import EntitiesPackage.Article;
import EntitiesPackage.Lot;
import EntitiesPackage.Vendeur;
import java.util.List;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.swing.text.LayoutQueue;

/**
 *
 * @author Thomas
 */
@WebService(serviceName = "WSManager")
@Stateless()
public class WSManager {

    EntityManager em;
    
    public WSManager(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysdisLibPU");
        em = emf.createEntityManager();
    }
    /**
     * Web service operation
     */
    @WebMethod(operationName = "getBestVendeur")
    public Vendeur getBestVendeur() {
        
        List<Vendeur> vendeurs = em.createNamedQuery("Vendeur.findAll",Vendeur.class).getResultList();
        
        Vendeur bestVendeur;
        
        if(vendeurs.size() == 0)
            return null;
        
        bestVendeur = vendeurs.get(0);
        
        for(int i=1;i<vendeurs.size();i++){
            if(getRentaVendeur(bestVendeur.getId())<getRentaVendeur(vendeurs.get(i).getId()))
                bestVendeur = vendeurs.get(i);
        }
        
        return bestVendeur;
        
    }
    
    private double getRentaVendeur(int idvendeur){
        
        List<Lot> lots = getListLotByVendeur(idvendeur);
        
        double renta = 0;
        
        for(int i=0;i<lots.size();i++)
            renta += getRentaLot(lots.get(i).getId());
        
        return renta;
            
    }
    
    private List<Lot> getListLotByVendeur(int idvendeur){
        return em.createNamedQuery("Lot.findByVendeur",Lot.class).setParameter("idVendeur", idvendeur).getResultList();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getBestLotPrice")
    public Lot getBestLotPrice() {
        
        List<Lot> lots = getListLot();
        
        
        if(lots.size() == 0)
            return null;
        
        Lot bestLot = lots.get(0);
        
        for(int i=1;i<lots.size();i++){
            if(getRentaLot(bestLot.getId())<getRentaLot(lots.get(i).getId()))
                bestLot = lots.get(i);
        }
        
        return bestLot;
        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getBestLotPourcentage")
    public Lot getBestLotPourcentage() {
        List<Lot> lots = getListLot();
        
        if(lots.size() == 0)
            return  null;
        
        Lot bestLot = lots.get(0);
        
        for(int i=1;i<lots.size();i++){
            if(getLotPourcentage(bestLot.getId()) < getLotPourcentage(lots.get(i).getId()))
                bestLot = lots.get(i);
        }
        
        return bestLot;
    }
    
    private double getLotPourcentage(int idLot){
        
        double benef;
        double prixachat = 0;
        
        List<Article> articles = em.createNamedQuery("Article.findByLot").setParameter("idLot", idLot).getResultList();
        
        benef = getRentaLot(idLot);
        prixachat = em.createNamedQuery("Lot.findById",Lot.class).setParameter("id", idLot).getSingleResult().getPrixAchat().doubleValue();
        
        return benef/prixachat*100;

        
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getListLot")
    public List<Lot> getListLot() {
        return em.createNamedQuery("Lot.findAll").getResultList();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getRentaLot")
    public double getRentaLot(@WebParam(name = "idLot") int idLot) {
        double sum=0;
        
        List<Article> articles = em.createNamedQuery("Article.findByLot").setParameter("idLot", idLot).getResultList();
        for(int i=0;i<articles.size();i++)
        {
            if(articles.get(i).getPrixVente() != null)
                sum += articles.get(i).getPrixVente().doubleValue();
        }
        return sum - em.createNamedQuery("Lot.findById",Lot.class).setParameter("id", idLot).getSingleResult().getPrixAchat().doubleValue();
    }

    /**
     * Web service operation
     */
    @WebMethod(operationName = "getFullRentaLot")
    public double getFullRentaLot(@WebParam(name = "idLot") int idLot) {
        
        double sum=0;
        
        List<Article> articles = em.createNamedQuery("Article.findByLot").setParameter("idLot", idLot).getResultList();
        for(int i=0;i<articles.size();i++)
        {
            sum +=articles.get(i).getPrixAnnonce().doubleValue()*1.5;
        }
        return sum - em.createNamedQuery("Lot.findById",Lot.class).setParameter("id", idLot).getSingleResult().getPrixAchat().doubleValue();
    }


    
}
