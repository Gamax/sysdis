/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MDB;

import MyMessageLib.*;
import MyMessageLib.MessageConstant;
import EntitiesPackage.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Thomas
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/VenteTopic")
    ,
        @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/VenteTopic")
    ,
        @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/VenteTopic")
    ,
        @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
    ,
        @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "DestinationType = 3")
})
public class MDBLog implements MessageListener {
    
    EntityManager em;
    
    MessageHandler mh;
    
    public MDBLog() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysdisLibPU");
        em = emf.createEntityManager();
        mh = new MessageHandler();
    }
    
    @Override
    public void onMessage(Message message) {
        
        try {
            mh.updateJmsMessage(message);
            
            if(mh.getMessageType() != MessageConstant.LOGTYPE)
            {
                Log("[MDBLog] MessageType is not a LogMessage !");
            }
            else
            {
                LogMessage logMsg = mh.getLogMessage();
                Log(logMsg.getText());
            }
        } catch (JMSException ex) {
            Logger.getLogger(MDBLog.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MyMessageException ex) {
            Logger.getLogger(MDBLog.class.getName()).log(Level.SEVERE, null, ex);
        }       
        
    }
    
    private void Log(String message){
        
        Logs logEntity = new Logs("[" + (new Date()).toString() + "]" + message);
        
        em.getTransaction().begin();
        em.persist(logEntity);
        em.getTransaction().commit();
    }
    
}
