/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package MDB;

import EJB.VenteSessionBean;
import EntitiesPackage.Acheteur;
import EntitiesPackage.Article;
import MyMessageLib.*;
import java.math.BigDecimal;
import java.math.MathContext;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.ActivationConfigProperty;
import javax.ejb.MessageDriven;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Thomas
 */
@MessageDriven(activationConfig = {
    @ActivationConfigProperty(propertyName = "clientId", propertyValue = "jms/VenteTopic")
        ,
    @ActivationConfigProperty(propertyName = "destinationLookup", propertyValue = "jms/VenteTopic")
        ,
    @ActivationConfigProperty(propertyName = "subscriptionName", propertyValue = "jms/VenteTopic")
        ,
    @ActivationConfigProperty(propertyName = "destinationType", propertyValue = "javax.jms.Topic")
        ,
    @ActivationConfigProperty(propertyName = "messageSelector", propertyValue = "DestinationType = 2")
})
public class MDBEncheres implements MessageListener {
    
    @Resource(mappedName = "jms/VenteTopic")
    private Topic venteTopic;
    
    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;
    
    EntityManager em;
    
    MessageHandler mh;
    
    public MDBEncheres() {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysdisLibPU");
        em = emf.createEntityManager();
        mh = new MessageHandler();
    }
    
    @Override
    public void onMessage(Message message) {
        
        try{
            mh.updateJmsMessage(message);
            
            switch(mh.getMessageType()){
                case MessageConstant.MAKEOFFERTYPE:
                    onMakeOffer(mh.getMakeOfferMessage());
                    return;
                case MessageConstant.ACCEPTORREFUSEOFFERTYPE:
                    onAcceptOrRefuse(mh.getAcceptOrRefuseOfferMessage());
                    return;
                default:
                    SendLog("Invalid MessageType Received");
                    return;
            }
            
        } catch (JMSException ex) {
            Logger.getLogger(MDBLog.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MyMessageException ex) {
            Logger.getLogger(MDBLog.class.getName()).log(Level.SEVERE, null, ex);
        } 
        
    }
    
    private void onMakeOffer(MakeOfferMessage msg)
    {
        Article article = em.createNamedQuery("Article.findById",Article.class).setParameter("id", msg.getIdArticle()).getSingleResult(); //recup de l'article concerné
        
        if(article.getDateVente() != null)
            return; //article déjà vendu on ne fait rien;
        
        Message msgToSend;
        
        //les 3 cas
        /***********************************************
         * /* l'offre est supérieur à 150% du prix anoncé *
         ************************************************/
        if(msg.getMontant() > article.getPrixAnnonce().doubleValue() * 1.5){
            
            //enregistrement vente dans bd
            article.setPrixVente(new BigDecimal(msg.getMontant(),MathContext.DECIMAL32));
            article.setDateVente(new Date());
            article.setRefAcheteur(em.createNamedQuery("Acheteur.findById",Acheteur.class).setParameter("id", msg.getIdAcheteur()).getSingleResult());
            
            em.getTransaction().begin();
            em.persist(article);
            em.getTransaction().commit();
            
            //envoi message acheteur
            
            msgToSend = context.createMessage();
            
            NotifyAcceptOrRefuseOfferMessage notifyAcceptOrRefuseOfferMessage = new NotifyAcceptOrRefuseOfferMessage(article.getId(), article.getRefAcheteur().getId(),article.getPrixVente().doubleValue(), true);
            
            try {
                notifyAcceptOrRefuseOfferMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            //envoi message vendeur
            
            msgToSend = context.createMessage();
            
            NotifyArticleSoldMessage notifyArticleSoldMessage = new NotifyArticleSoldMessage(article.getRefLot().getRefVendeur().getId(), article.getRefAcheteur().getId(), article.getId(), article.getPrixVente().doubleValue());
            
            try {
                notifyArticleSoldMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            SendLog("Acheteur " + msg.getIdAcheteur() + " a envoyé une offre sur " + msg.getIdArticle() + " de "+msg.getMontant()
                    +" € et à remporter l'enchère par un dépassement de 150% du prix annoncé : " + article.getPrixAnnonce() +" €");
            
            return;
        }
        
        /****************************************************
         * /* l'offre est inférieure au prix anoncé ou enchère *
         *****************************************************/
        
        if(msg.getMontant() < article.getPrixAnnonce().doubleValue() || (article.getPrixVente() != null && msg.getMontant() < article.getPrixVente().doubleValue())){ //todo et si c'ets la première offre ? le prix vente est égal à nul ?
            
            //envoi message vers acheteur
            msgToSend = context.createMessage();
            
            NotEnoughOfferMessage notEnoughOfferMessage = new NotEnoughOfferMessage(article.getId(), msg.getIdAcheteur(), msg.getMontant());
            
            try{
                notEnoughOfferMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            SendLog("Acheteur " + msg.getIdAcheteur() + " a envoyé une offre sur " + msg.getIdArticle() + " de "+msg.getMontant()
                    +" € , c'est insuffisant, l'offre en cours est  " + article.getPrixVente() +" €");
            
            return;
        }
        
        /*************************************************
         * /* l'offre est supérieur à la meilleure offre *
         *************************************************/
        
        if(article.getPrixVente() == null || msg.getMontant() > article.getPrixVente().doubleValue()){
            
            //changement leader de l'enchère
            double prixVente;
            
            if(article.getPrixVente() != null)
            {
                prixVente = article.getPrixVente().doubleValue(); 
            }
            else
            {
                prixVente = 0;
            }
                
            
            article.setPrixVente(new BigDecimal(msg.getMontant(),MathContext.DECIMAL32));
            article.setRefAcheteur(em.createNamedQuery("Acheteur.findById",Acheteur.class).setParameter("id", msg.getIdAcheteur()).getSingleResult());
            
            em.getTransaction().begin();
            em.persist(article);
            em.getTransaction().commit();
            
            //envoie message vers vendeur
            
            msgToSend = context.createMessage();
            
            NotifyMakeOfferMessage notifyMakeOfferMessage = new NotifyMakeOfferMessage(msg.getIdArticle(), msg.getIdAcheteur(), article.getRefLot().getRefVendeur().getId(), msg.getMontant());
            try{
                notifyMakeOfferMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            SendLog("Acheteur " + msg.getIdAcheteur() + " a envoyé une offre sur " + msg.getIdArticle() + " de "+msg.getMontant()
                    +" € et devient l'offre en cours ! Offre précédente " + prixVente +" €");
            
            return;
        }
    }
    
    private void onAcceptOrRefuse(AcceptOrRefuseOfferMessage msg)
    {
        Article article = em.createNamedQuery("Article.findById",Article.class).setParameter("id", msg.getIdArticle()).getSingleResult(); //recup de l'article concerné
        
        if(article.getDateVente() != null)
            return; //article déjà vendu on ne fait rien;
        
        Message msgToSend;
        
        if(msg.isIsAccepted()){
            //offre acceptée
            article.setPrixVente(new BigDecimal(msg.getMontant(),MathContext.DECIMAL32));
            article.setDateVente(new Date());
            article.setRefAcheteur(em.createNamedQuery("Acheteur.findById",Acheteur.class).setParameter("id", msg.getIdAcheteur()).getSingleResult());
            
            em.getTransaction().begin();
            em.persist(article);
            em.getTransaction().commit();
            
            //envoi message vers acheteur
            msgToSend = context.createMessage();
            
            NotifyAcceptOrRefuseOfferMessage notifyAcceptOrRefuseOfferMessage = new NotifyAcceptOrRefuseOfferMessage(msg.getIdArticle(),msg.getIdAcheteur(), msg.getMontant(), true);
            
            try{
                notifyAcceptOrRefuseOfferMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            SendLog("L'offre de l'acheteur  " + msg.getIdAcheteur() + " sur l'article " + msg.getIdArticle() 
                    + "d'un montant de " + msg.getMontant() + " € a reçu une réponse : " + msg.isIsAccepted());
        }
        else
        {
            //envoi message vers acheteur
            msgToSend = context.createMessage();
            
            NotifyAcceptOrRefuseOfferMessage notifyAcceptOrRefuseOfferMessage = new NotifyAcceptOrRefuseOfferMessage(msg.getIdArticle(),msg.getIdAcheteur(), msg.getMontant(), false);
            
            try{
                notifyAcceptOrRefuseOfferMessage.Fill(msgToSend);
            } catch (JMSException ex) {
                Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            context.createProducer().send(venteTopic, msgToSend);
            
            SendLog("L'offre de l'acheteur  " + msg.getIdAcheteur() + " sur l'article " + msg.getIdArticle() 
                    + "d'un montant de " + msg.getMontant() + " € a reçu une réponse : " + msg.isIsAccepted());
        }
    }
    
    private void SendLog(String text)
    {
        Message message = context.createMessage();
        
        LogMessage logmsg = new LogMessage("[MDBEncheres]"+text);
        
        System.out.println("[MDBEncheres]"+text);
        
        try {
            logmsg.Fill(message);
        } catch (JMSException ex) {
            Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        context.createProducer().send(venteTopic, message);
    }
}
