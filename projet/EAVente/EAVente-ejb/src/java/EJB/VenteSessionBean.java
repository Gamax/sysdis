/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import EntitiesPackage.Acheteur;
import EntitiesPackage.Article;
import EntitiesPackage.Lot;
import EntitiesPackage.Vendeur;
import MyMessageLib.AcceptOrRefuseOfferMessage;
import MyMessageLib.LogMessage;
import MyMessageLib.MakeOfferMessage;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.jms.JMSConnectionFactory;
import javax.jms.JMSContext;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Topic;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.NoResultException;
import javax.persistence.Persistence;

/**
 *
 * @author Thomas
 */



@Stateless
public class VenteSessionBean implements VenteSessionBeanRemote {

    @Resource(mappedName = "jms/VenteTopic")
    private Topic venteTopic;

    @Inject
    @JMSConnectionFactory("java:comp/DefaultJMSConnectionFactory")
    private JMSContext context;

    @Override
    public Vendeur loginVendeur(String login) {
        
        EntityManager em = getEntityManager();

        Vendeur retour;
        
        try{
        retour = em.createNamedQuery("Vendeur.findByLogin",Vendeur.class).setParameter("login", login).getSingleResult();
        }
        catch(NoResultException e){
            SendLog("Tentative de connexion échouée avec (" +login+")");
            return null;
        }
        
        SendLog("Le vendeur " + retour.getPrenom() +" " +retour.getNom() +" ("+retour.getLogin()+") s'est connecté");
        
        return retour;
        
    }
    
    @Override
    public Acheteur loginAcheteur(String login) {
        EntityManager em = getEntityManager();

        Acheteur retour;
        
        try{
        retour = em.createNamedQuery("Acheteur.findByLogin",Acheteur.class).setParameter("login", login).getSingleResult();
        }
        catch(NoResultException e){
            SendLog("Tentative de connexion échouée avec (" +login+")");
            return null;
        }
        
        SendLog("L'acheteur " + retour.getPrenom() +" " +retour.getNom() +" ("+retour.getLogin()+") s'est connecté");
        
        return retour;
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void addLot(Lot lot) {
        EntityManager em = getEntityManager();
        
        em.getTransaction().begin();
        
        em.persist(lot);
        
        em.getTransaction().commit();
        
        em.close();
        
        SendLog("Lot ("+lot.getDescriptif() +") a été ajouté par "+lot.getRefVendeur().getLogin());
        
    }

    @Override
    public void addArticle(Article article) {
        EntityManager em = getEntityManager();
        
        em.getTransaction().begin();
        
        em.persist(article);
        
        em.getTransaction().commit();
        
        em.close();
        
        SendLog("Article ("+article.getDescription()+") a été ajouté par "+article.getRefLot().getRefVendeur().getLogin());
    }
    
    @Override
    public List<Lot> getLots(int idVendeur) {
        EntityManager em = getEntityManager();
        
        List<Lot> listLot = em.createNamedQuery("Lot.findByVendeur",Lot.class).setParameter("idVendeur", idVendeur).getResultList();
        
        return listLot;
    }

    @Override
    public List<Article> getArticlesForVendeur(int idVendeur) {
        EntityManager em = getEntityManager();
        
        List<Article> listArticle = em.createNamedQuery("Article.findByVendeur",Article.class).setParameter("idVendeur", idVendeur).getResultList();
        
        return listArticle;
    }

    @Override
    public List<Article> getArticlesForAcheteur() {
         EntityManager em = getEntityManager();
        
        return em.createNamedQuery("Article.findUnSold",Article.class).getResultList();
    }

    @Override
    public void makeOffer(int idArticle, int idAcheteur, double montant) {
        
        Message message = context.createMessage();
        MakeOfferMessage makeOfferMessage = new MakeOfferMessage(idArticle, idAcheteur, montant);
        
        try{
            makeOfferMessage.Fill(message);
        } catch (JMSException ex) {
            Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        context.createProducer().send(venteTopic, message);
        
        SendLog("Acheteur " + idAcheteur + " a envoyé une offre sur " + idArticle + " de "+montant+" €");
    }
    
    @Override
    public void AcceptOrRefuseOffer(int idArticle, int idAcheteur, double montant, boolean isAccepted) {
        Message message = context.createMessage();
        AcceptOrRefuseOfferMessage acceptOrRefuseOfferMessage = new AcceptOrRefuseOfferMessage(idArticle, idAcheteur, montant, isAccepted);
        
        try{
            acceptOrRefuseOfferMessage.Fill(message);
        } catch (JMSException ex) {
            Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        context.createProducer().send(venteTopic, message);
        
        SendLog("L'offre de l'acheteur  " + idAcheteur + " sur l'article " + idArticle + "d'un montant de " + montant + " € a reçu une réponse : " + isAccepted);
                
    }
    
    private EntityManager getEntityManager(){
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("SysdisLibPU");
        return emf.createEntityManager();
    }
    
    private void SendLog(String text)
    {
        Message message = context.createMessage();
        
        LogMessage logmsg = new LogMessage("[EJB-Vente]"+text);
        
        System.out.println("[EJB-Vente]"+text);
        
        try {
            logmsg.Fill(message);
        } catch (JMSException ex) {
            Logger.getLogger(VenteSessionBean.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        context.createProducer().send(venteTopic, message);
    }

    

    

    
}
