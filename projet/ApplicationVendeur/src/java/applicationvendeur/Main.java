/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationvendeur;

import EJB.VenteSessionBeanRemote;
import GUI.Controller.CVendeur;
import GUI.GUILogin;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;

/**
 *
 * @author Thomas
 */
public class Main {

    @EJB
    private static VenteSessionBeanRemote venteSessionBean;

    @Resource(mappedName = "jms/VenteTopic")
    private static Topic venteTopic;

    @Resource(mappedName = "jms/VenteTopicFactory")
    private static ConnectionFactory venteTopicFactory;

    private static Connection con = null;
    private static Session ses = null;
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        try {
            GUILogin login = new GUILogin(null, true);
            EntitiesPackage.Vendeur vendeur = null ;
            do{
                login.show();
                if(login.getEndState()==true)
                {
                    vendeur = venteSessionBean.loginVendeur(login.getUser());
                }
                else
                    System.exit(0);
            }while(vendeur == null);
            
            con = venteTopicFactory.createConnection();
            ses = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
            con.start();
            
            
            CVendeur controller ;
            controller = new CVendeur(vendeur,venteSessionBean, venteTopic, con, ses);
            controller.init();
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
