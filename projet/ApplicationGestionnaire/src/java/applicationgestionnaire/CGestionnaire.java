/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationgestionnaire;

import java.util.List;
import java.util.Vector;
import javax.swing.table.DefaultTableModel;
import ws.Lot;
import ws.Vendeur;

/**
 *
 * @author Steph
 */
public class CGestionnaire {
    private GUIGestionnaire gui ; 
    
    public CGestionnaire()
    {
        
    }
    
    public void init()
    {
        gui = new GUIGestionnaire();
        gui.setController(this);
        gui.setVisible(true);
    }
    
    public void getBestSeller()
    {
       Vendeur vendeur = getBestVendeur();
        
       gui.bestSeller(vendeur.getNom(), vendeur.getPrenom());
    }
    
    public void getBestLotPercent()
    {
        Lot lot = getBestLotPourcentage();
        
        gui.BestLotPercent(String.valueOf(lot.getId()), String.valueOf(lot.getDescriptif()), "not implemented yet");
    }
    
    public void getBestLotNet()
    {
        Lot lot = getBestLotPrice();
        
        gui.BestLotNet(String.valueOf(lot.getId()), String.valueOf(lot.getDescriptif()), "not implemented yet");
    }
    
    public void getLots()
    {
        DefaultTableModel dtm = new DefaultTableModel();
        
        List<Lot> lots = getListLot();
        
        dtm.addColumn("Num�ro");
        dtm.addColumn("Description");
        dtm.addColumn("B�n�fices acquis");
        dtm.addColumn("B�n�fices possibles");
        
        Vector vec ;
        Lot lot ;
        for(int i = 0 ; i < lots.size() ; i++)
        {
            vec = new Vector();
            lot = lots.get(i);
            
            vec.add(lot.getId());
            vec.add(lot.getDescriptif());
            vec.add(getRentaLot(lot.getId()));
            vec.add(getFullRentaLot(lot.getId()));
            dtm.addRow(vec);
        }
        
        gui.setLots(dtm);
    }

    private static Lot getBestLotPourcentage() {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getBestLotPourcentage();
    }

    private static Lot getBestLotPrice() {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getBestLotPrice();
    }

    private static Vendeur getBestVendeur() {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getBestVendeur();
    }

    private static double getFullRentaLot(int idLot) {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getFullRentaLot(idLot);
    }

    private static java.util.List<ws.Lot> getListLot() {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getListLot();
    }

    private static double getRentaLot(int idLot) {
        ws.WSManager_Service service = new ws.WSManager_Service();
        ws.WSManager port = service.getWSManagerPort();
        return port.getRentaLot(idLot);
    }
    
    
}

