/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationacheteur;

import EJB.VenteSessionBeanRemote;
import EntitiesPackage.Acheteur;
import GUI.Controller.CAcheteur;
import GUI.GUILogin;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.Topic;

/**
 *
 * @author Thomas
 */
public class Main {

    @EJB
    private static VenteSessionBeanRemote venteSessionBean;

    @Resource(mappedName = "jms/VenteTopic")
    private static Topic venteTopic;

    @Resource(mappedName = "jms/VenteTopicFactory")
    private static ConnectionFactory venteTopicFactory;

    private static Connection con = null;
    private static Session ses = null;
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            GUILogin login = new GUILogin(null, true);
            Acheteur acheteur = null ;
            do{
                login.show();
                if(login.getEndState()==true)
                {
                    acheteur = venteSessionBean.loginAcheteur(login.getUser());
                }
                else
                    System.exit(0);
            }while(acheteur == null);
            
            con = venteTopicFactory.createConnection();
            ses = con.createSession(false, Session.AUTO_ACKNOWLEDGE);
            con.start();
            
            CAcheteur controller ;
            controller = new CAcheteur(acheteur,venteSessionBean, venteTopic, con, ses);
            controller.init();
        } catch (JMSException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    
    
}
