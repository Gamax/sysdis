/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import java.util.StringTokenizer;
import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */
public class MakeOfferMessage extends MyMessage{

    int idArticle;
    int idAcheteur;
    double montant;

    public int getIdArticle() {
        return idArticle;
    }

    public int getIdAcheteur() {
        return idAcheteur;
    }

    public double getMontant() {
        return montant;
    }
    
    public MakeOfferMessage(int idArticle, int idAcheteur, double montant) {
        this.idArticle = idArticle;
        this.idAcheteur = idAcheteur;
        this.montant = montant;
    }

    public MakeOfferMessage(Message message) throws JMSException, MyMessageException
    {
        if(message.getIntProperty("MessageType") != MessageConstant.MAKEOFFERTYPE)
            throw new MyMessageException("Invalid Message Type !");
        
        idArticle = message.getIntProperty("idArticle");
        idAcheteur = message.getIntProperty("idAcheteur");
        montant = message.getDoubleProperty("montant");
    }
    
    @Override
    public void Fill(Message message) throws JMSException
    {
        setDestination(MessageConstant.TOENCHERE, message);
        setType(MessageConstant.MAKEOFFERTYPE, message);
        
        message.setIntProperty("idArticle", idArticle);
        message.setIntProperty("idAcheteur", idAcheteur);
        message.setDoubleProperty("montant", montant);
        
    }
    
    
        
    
}
