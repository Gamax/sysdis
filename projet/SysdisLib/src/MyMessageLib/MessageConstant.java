/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

/**
 *
 * @author Thomas
 */
public class MessageConstant {
    public static final int TOVENDEUR = 0;
    public static final int TOACHETEUR = 1;
    public static final int TOENCHERE = 2;
    public static final int TOLOG = 3;
    
    public static final int MAKEOFFERTYPE = 0; //acheteur fait offre sur article
    public static final int NOTIFYMAKEOFFERTYPE = 1; //prévenir vendeur offre faite
    public static final int ACCEPTORREFUSEOFFERTYPE = 2; //vendeur accepte ou refuse une offre
    public static final int NOTIFYACCEPTORREFUSEOFFERTYPE = 3; //prévenir acheteur offre acceptée par vendeur (aussi auto) ou refusée 
    public static final int NOTENOUGHOFFERTYPE = 4; //prévenir le client que son offre est insufisante
    public static final int LOGTYPE = 5; //log message
    public static final int NOTIFYARTICLESOLDTYPE = 6; //notifie le vendeur quand un article ets vendu
}
