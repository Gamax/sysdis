/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */
public class NotifyMakeOfferMessage extends MyMessage{

    int idArticle;
    int idAcheteur;
    int idDestVendeur;
    double montant;

    public int getIdArticle() {
        return idArticle;
    }

    public int getIdAcheteur() {
        return idAcheteur;
    }

    public int getIdDestVendeur() {
        return idDestVendeur;
    }

    public double getMontant() {
        return montant;
    }
    
    public NotifyMakeOfferMessage(int idArticle, int idAcheteur, int idDestVendeur, double montant) {
        this.idArticle = idArticle;
        this.idAcheteur = idAcheteur;
        this.idDestVendeur = idDestVendeur;
        this.montant = montant;
    }

    public NotifyMakeOfferMessage(Message message) throws JMSException, MyMessageException
    {
        if(message.getIntProperty("MessageType") != MessageConstant.NOTIFYMAKEOFFERTYPE)
            throw new MyMessageException("Invalid Message Type !");
        
        idArticle = message.getIntProperty("idArticle");
        idAcheteur = message.getIntProperty("idAcheteur");
        idDestVendeur = message.getIntProperty("idDestVendeur");
        montant = message.getDoubleProperty("montant");
    }
    
    @Override
    public void Fill(Message message) throws JMSException {
        setDestination(MessageConstant.TOVENDEUR, message);
        setType(MessageConstant.NOTIFYMAKEOFFERTYPE, message);
        
        message.setIntProperty("idArticle", idArticle);
        message.setIntProperty("idAcheteur", idAcheteur);
        message.setIntProperty("idDestVendeur", idDestVendeur);
        message.setDoubleProperty("montant", montant);
    }
    
}
