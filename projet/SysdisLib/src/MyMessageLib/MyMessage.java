/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.TextMessage;

/**
 *
 * @author Thomas
 */

//syntaxe idDestination:idMessageType$param1$param2$...

public abstract class MyMessage {
    
    public void setDestination(int toDest,Message jmsMessage) throws JMSException
    {
        jmsMessage.setIntProperty("DestinationType", toDest);
    }
    
    public void setType(int type,Message jmsMessage) throws JMSException
    {
        jmsMessage.setIntProperty("MessageType", type);
    }
    
    public abstract void Fill(Message message) throws JMSException;
    
    
}
