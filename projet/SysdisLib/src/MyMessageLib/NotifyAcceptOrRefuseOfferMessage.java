/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */
public class NotifyAcceptOrRefuseOfferMessage extends MyMessage {
    private int idArticle;
    private int idAcheteur;
    private double montant;
    private boolean isAccepted;

    public int getIdArticle() {
        return idArticle;
    }

    public double getMontant() {
        return montant;
    }

    public boolean isIsAccepted() {
        return isAccepted;
    }

    public int getIdAcheteur() {
        return idAcheteur;
    }
    
    

    public NotifyAcceptOrRefuseOfferMessage(int idArticle,int idAcheteur, double montant, boolean isAccepted) {
        this.idArticle = idArticle;
        this.montant = montant;
        this.isAccepted = isAccepted;
        this.idAcheteur = idAcheteur;
    }
    
    public NotifyAcceptOrRefuseOfferMessage(Message message) throws JMSException, MyMessageException
    {
        if(message.getIntProperty("MessageType") != MessageConstant.NOTIFYACCEPTORREFUSEOFFERTYPE)
            throw new MyMessageException("Invalid Message Type !");
        
        idArticle = message.getIntProperty("idArticle");
        idAcheteur = message.getIntProperty("idAcheteur");
        montant = message.getDoubleProperty("montant");
        isAccepted = message.getBooleanProperty("isAccepted");
    }

    @Override
    public void Fill(Message message) throws JMSException {
        setDestination(MessageConstant.TOACHETEUR, message);
        setType(MessageConstant.NOTIFYACCEPTORREFUSEOFFERTYPE, message);
        
        message.setIntProperty("idArticle", idArticle);
        message.setIntProperty("idAcheteur", idAcheteur);
        message.setDoubleProperty("montant", montant);
        message.setBooleanProperty("isAccepted", isAccepted);
        
    }
    
    
}
