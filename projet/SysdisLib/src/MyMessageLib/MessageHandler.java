/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */

/*
Attribute name :
MessageType int - type de message envoyé
Le reste est définit dans chaque classe
*/

public class MessageHandler {
    
    private Message jmsMessage;
    
    public void updateJmsMessage(Message m){
        jmsMessage = m;
    }
    
    public int getMessageType() throws JMSException
    {
        return jmsMessage.getIntProperty("MessageType");
    }
    
    public MakeOfferMessage getMakeOfferMessage() throws JMSException, MyMessageException
    {
        return new MakeOfferMessage(jmsMessage);
    }
    
    public NotifyMakeOfferMessage getNotifyMakeOfferMessage() throws JMSException, MyMessageException
    {
        return new NotifyMakeOfferMessage(jmsMessage);
    }
    
    public AcceptOrRefuseOfferMessage getAcceptOrRefuseOfferMessage() throws JMSException, MyMessageException
    {
        return new AcceptOrRefuseOfferMessage(jmsMessage);
    }
    
    public NotifyAcceptOrRefuseOfferMessage getNotifyAcceptOrRefuseOfferMessage() throws JMSException, MyMessageException
    {
        return new NotifyAcceptOrRefuseOfferMessage(jmsMessage);
    }
    
    public NotEnoughOfferMessage getNotEnoughOfferMessage() throws JMSException, MyMessageException
    {
        return new NotEnoughOfferMessage(jmsMessage);
    }
    
    public LogMessage getLogMessage() throws JMSException, MyMessageException
    {
        return new LogMessage(jmsMessage);
    }
    
    public NotifyArticleSoldMessage getNotifyArticleSoldMessage() throws JMSException, MyMessageException 
    {
        return new NotifyArticleSoldMessage(jmsMessage);
    }
    
    
}
