/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

/**
 *
 * @author Thomas
 */
public class MyMessageException extends Exception{
    
    public MyMessageException(String message){
        super(message);
    }
    
}
