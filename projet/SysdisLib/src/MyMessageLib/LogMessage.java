/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */
public class LogMessage extends MyMessage{
    private String text;

    public String getText() {
        return text;
    }
    
    public LogMessage(String text) {
        this.text = text;
    }
    
    public LogMessage(Message message) throws JMSException, MyMessageException
    {
        if(message.getIntProperty("MessageType") != MessageConstant.LOGTYPE)
            throw new MyMessageException("Invalid Message Type !");
        
        text = message.getStringProperty("text");
    }

    @Override
    public void Fill(Message message) throws JMSException {
        setDestination(MessageConstant.TOLOG, message);
        setType(MessageConstant.LOGTYPE, message);
        
        message.setStringProperty("text", text);
        
    }
    
    
    
}
