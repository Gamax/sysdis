/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyMessageLib;

import javax.jms.JMSException;
import javax.jms.Message;

/**
 *
 * @author Thomas
 */
public class NotifyArticleSoldMessage extends MyMessage{
    private int idVendeur;
    private int idAcheteur;
    private int idArticle;
    private double montant;

    public int getIdVendeur() {
        return idVendeur;
    }

    public int getIdAcheteur() {
        return idAcheteur;
    }

    public int getIdArticle() {
        return idArticle;
    }

    public double getMontant() {
        return montant;
    }

    public NotifyArticleSoldMessage(int idVendeur, int idAcheteur, int idArticle, double montant) {
        this.idVendeur = idVendeur;
        this.idAcheteur = idAcheteur;
        this.idArticle = idArticle;
        this.montant = montant;
    }
    
    public NotifyArticleSoldMessage(Message message) throws JMSException, MyMessageException{
        
        if(message.getIntProperty("MessageType") != MessageConstant.NOTIFYARTICLESOLDTYPE)
            throw new MyMessageException("Invalid Message Type !");
        
        idVendeur = message.getIntProperty("idVendeur");
        idAcheteur = message.getIntProperty("idAcheteur");
        idArticle = message.getIntProperty("idArticle");
        montant = message.getDoubleProperty("montant");

    }

    @Override
    public void Fill(Message message) throws JMSException {
        setDestination(MessageConstant.TOVENDEUR, message);
        setType(MessageConstant.NOTIFYARTICLESOLDTYPE, message);
        
        message.setIntProperty("idVendeur", idVendeur);
        message.setIntProperty("idAcheteur", idAcheteur);
        message.setIntProperty("idArticle", idArticle);
        message.setDoubleProperty("montant", montant);
    }
    
    
}
