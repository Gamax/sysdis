/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Messages;

/**
 *
 * @author Steph
 */
public class Proposition {
    private int Article ; 
    private int Acheteur ;
    private double Prix ;
    private String Refus ;
    
    
    public Proposition()
    {
        
    }
    
    public Proposition(int article, int acheteur, double prix, String refus)
    {
        Article = article ;
        Acheteur = acheteur ;
        Prix = prix ;
        Refus = refus ;
    }
    
    public void setRefus(String refus)
    {
        Refus = refus ;
    }
    
    public void setPrix(double prix)
    {
        Prix = prix ;
    }
    
    public void setAcheteur(int acheteur)
    {
        Acheteur = acheteur ;
    }
    
    public void setArticle(int article)
    {
        Article = article ;
    }
    
    public String getRefus()
    {
        return Refus ;
    }
    
    public double getPrix()
    {
        return Prix ;
    }
    
    public int getAcheteur()
    {
        return Acheteur ;
    }
    
    public int getArticle()
    {
        return Article ;
    }
}
