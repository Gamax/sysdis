/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EJB;

import EntitiesPackage.Acheteur;
import EntitiesPackage.Article;
import EntitiesPackage.Lot;
import EntitiesPackage.Vendeur;
import java.util.List;
import javax.ejb.Remote;

/**
 *
 * @author Thomas
 */
@Remote
public interface VenteSessionBeanRemote {

    Vendeur loginVendeur(String login);

    void addLot(Lot lot);

    void addArticle(Article article);
    
    List<Article> getArticlesForVendeur(int IdVendeur);

    List<Article> getArticlesForAcheteur();

    void makeOffer(int idArticle, int idAcheteur, double montant);

    Acheteur loginAcheteur(String login);

    List<Lot> getLots(int idVendeur);

    void AcceptOrRefuseOffer(int idArticle, int idAcheteur, double montant, boolean isAccepted);
    
}
