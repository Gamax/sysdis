/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import EJB.VenteSessionBeanRemote;
import EntitiesPackage.*;
import Messages.Proposition;
import GUI.GUIVendeur;
import MyMessageLib.MessageConstant;
import MyMessageLib.MessageHandler;
import MyMessageLib.MyMessageException;
import MyMessageLib.NotifyArticleSoldMessage;
import MyMessageLib.NotifyMakeOfferMessage;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Vector;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Steph
 */
public class CVendeur implements MessageListener {
    
    private Vendeur VendeurRef ; 
    private GUIVendeur gui ;

    private VenteSessionBeanRemote EJB ;
    private List<Lot> lots ;
    private MessageHandler mh ;
    private MessageConsumer MessageReceive = null;
    
    public CVendeur (Vendeur vendeur, VenteSessionBeanRemote ejb, Topic topic,  Connection con, Session sess) throws JMSException
    {
        VendeurRef = vendeur ;
        EJB = ejb ;
        List<Lot> lots = new ArrayList<>();
        mh = new MessageHandler();
        MessageReceive = sess.createConsumer(topic);
        MessageReceive.setMessageListener(this);
    }
    
    public void init()
    {
        gui = new GUIVendeur();
        gui.setController(this);
        gui.setUsername(VendeurRef.getPrenom() +" "+VendeurRef.getNom()+" ("+VendeurRef.getLogin()+")");
        getListLots();
        setArticles();
        gui.setVisible(true);
    }
    
    public void addLot(String date, String prix, String descriptif)
    {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            Date ref = new Date();
            ref=sdf.parse(date);
            double price = Double.parseDouble(prix);
            
            Lot lot = new Lot();
            lot.setDateAchat(ref);
            lot.setPrixAchat(BigDecimal.valueOf(price));
            lot.setDescriptif(descriptif);
            lot.setRefVendeur(VendeurRef);
            
            EJB.addLot(lot);
            
            getListLots();
            gui.resetLot();
            
        } catch (ParseException ex) {
            JOptionPane.showMessageDialog(null, "Date invalide !");
        }
        catch(NumberFormatException e)
        {
            JOptionPane.showMessageDialog(null, "Prix invalide !");
        }
    }
    
    public void addArticle(String numLot, String prix, String descriptif, String photo)
    {
        
        try {
            double price = Double.parseDouble(prix);
            int NumLot = Integer.parseInt(numLot);
            
            Article article = new Article();
            
            
            article.setPrixAnnonce(BigDecimal.valueOf(price));
            article.setRefLot(getLot(NumLot));
            if(article.getRefLot() == null)
            {
                JOptionPane.showMessageDialog(null, "Lot invalide !");
                return ;
            }
            article.setDescription(descriptif);
            article.setPhotoPath(photo);
            
            EJB.addArticle(article);
            
            setArticles();
            gui.resetAchat();
        } 
        catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Prix invalide !");
        }
    }

    public Lot getLot(int IdLot)
    {
        Lot lot ;
        for(int i=0 ; i<lots.size() ; i++ )
        {
            lot = lots.get(i);
            if(lot.getId()==IdLot)
                return lot ;
        }
        return null ; 
    }
    
    public void acceptProposition(String article, String price, String Acheteur)
    {
        try
        {
            EJB.AcceptOrRefuseOffer(Integer.parseInt(article), Integer.parseInt(Acheteur), Double.parseDouble(price),  true);
            setArticles(); 
        }
        catch(NumberFormatException e)
        {
            System.out.println(e.getCause() + " : " + e.getMessage());
        }
    }
    
    public void refuseProposition(String article, String price, String Acheteur)
    {
        try
        {
            EJB.AcceptOrRefuseOffer(Integer.parseInt(article), Integer.parseInt(Acheteur), Double.parseDouble(price),  false);
            setArticles(); 
        }
        catch(NumberFormatException e)
        {
             System.out.println(e.getCause() + " : " + e.getMessage());
        }
    }


    
    private void setArticles()
    {
        List<Article> articles = EJB.getArticlesForVendeur(VendeurRef.getId());
        DefaultTableModel dtm = new DefaultTableModel();
        
        dtm.addColumn("Numéro de l'article");
        dtm.addColumn("Description");
        dtm.addColumn("Prix annoncé");
        dtm.addColumn("Meilleure offre");
        dtm.addColumn("Référence acheteur");
        
        Vector vec ;
        Article article ;
        for(int i = 0 ; i<articles.size(); i++)
        {
            vec = new Vector();
            article = articles.get(i);
            vec.add(article.getId());
            vec.add(article.getDescription());
            vec.add(article.getPrixAnnonce());
            if(article.getPrixVente()==null || article.getPrixVente().doubleValue() <= 0)
            {
                vec.add("Aucune pour le moment");
                vec.add("aucun pour le moment");
            }
            else
            {
                vec.add(article.getPrixVente().doubleValue());
                vec.add(article.getRefAcheteur().getId());
            }
            dtm.addRow(vec);
        }
        
        gui.setArticles(dtm);
    }
    
    
    
    private void getListLots()
    { 
        lots  = EJB.getLots(VendeurRef.getId()) ;
        
        Lot lot ;
        DefaultTableModel dtm = new DefaultTableModel();
        Vector vec ;
        
        dtm.addColumn("Numéro de lot");
        dtm.addColumn("Description");
        dtm.addColumn("Date d'achat");
        dtm.addColumn("Prix d'achat");
        
        if(lots!=null)
        {
            for(int i = 0 ; i<lots.size() ; i++)
            {
                lot = lots.get(i);
                vec = new Vector();

                vec.add(lot.getId());
                vec.add(lot.getDescriptif());
                vec.add(lot.getDateAchat());
                vec.add(lot.getPrixAchat());

                dtm.addRow(vec);
            }
        }
        
        
        gui.setLots(dtm);
    }
    
     @Override
    public void onMessage(Message message) {
        
        try {
            if(message.getIntProperty("DestinationType") != MessageConstant.TOVENDEUR)
                return;
            
            
            
            mh.updateJmsMessage(message);
            
            
            switch(mh.getMessageType())
            {
                case MessageConstant.NOTIFYMAKEOFFERTYPE : 
                    
                    OffreRecue(mh.getNotifyMakeOfferMessage());
                    break ;
                case MessageConstant.NOTIFYARTICLESOLDTYPE : OffreConclue(mh.getNotifyArticleSoldMessage());
                                                              break ;
            }
        } catch (JMSException |MyMessageException ex) {
            System.out.println(ex.getCause() + " : " + ex.getMessage());
        } 
    }
    
    public void checkOffres(String idArticle, String montant, String idAcheteur)
    {
        if(JOptionPane.showConfirmDialog(null, "Offre pour l'article : " + idArticle + " pour un montant de " + montant) == JOptionPane.OK_OPTION)
            acceptProposition(idArticle, montant, idAcheteur);
        else
            setArticles();
            
    }
    
    private void OffreRecue(NotifyMakeOfferMessage message)
    {
        if(message.getIdDestVendeur() != VendeurRef.getId())
            return; //on ne fait rien si le message ne nous est pas destiné
            
        if(JOptionPane.showConfirmDialog(null, "Offre pour l'article : " + message.getIdArticle() + " pour un montant de " + message.getMontant()) == JOptionPane.OK_OPTION)
            acceptProposition(String.valueOf(message.getIdArticle()), String.valueOf(message.getMontant()), String.valueOf(message.getIdAcheteur()));
        else
            refuseProposition(String.valueOf(message.getIdArticle()), String.valueOf(message.getMontant()), String.valueOf(message.getIdAcheteur()));
    }
    
    private void OffreConclue(NotifyArticleSoldMessage message)
    {
        if(message.getIdVendeur() != VendeurRef.getId())
            return; //on ne fait rien si le message ne nous est pas destiné
        
        JOptionPane.showMessageDialog(null, "Félicitations ! Votre article : " + message.getIdArticle() + " est vendu pour " + message.getMontant() + " €");
        setArticles();
    }
}
