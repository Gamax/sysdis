/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package GUI.Controller;

import EJB.VenteSessionBeanRemote;
import EntitiesPackage.Acheteur;
import EntitiesPackage.Article;
import GUI.GUIAcheteur;
import Messages.Proposition;
import MyMessageLib.AcceptOrRefuseOfferMessage;
import MyMessageLib.MessageConstant;
import MyMessageLib.MessageHandler;
import MyMessageLib.MyMessageException;
import MyMessageLib.NotEnoughOfferMessage;
import MyMessageLib.NotifyAcceptOrRefuseOfferMessage;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageConsumer;
import javax.jms.MessageListener;
import javax.jms.Session;
import javax.jms.Topic;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Steph
 */
public class CAcheteur implements MessageListener{
    
    private VenteSessionBeanRemote EJB ;
    private Acheteur AcheteurRef ;
    private List<Proposition> OffresAcceptees ;
    private List<Proposition> OffresRefusees ; 
    private GUIAcheteur gui; 
    private MessageHandler mh ;
    private MessageConsumer MessageReceive = null;
    
    public CAcheteur(Acheteur acheteur, VenteSessionBeanRemote ejb, Topic topic, Connection con, Session sess) throws JMSException
    {
        EJB = ejb ;
        AcheteurRef = acheteur ;
        mh = new MessageHandler();
        MessageReceive = sess.createConsumer(topic);
        MessageReceive.setMessageListener(this);
        OffresAcceptees = new ArrayList<>();
        OffresRefusees = new ArrayList<>();
    }
    
    public void init()
    {
        gui = new GUIAcheteur();
        gui.setController(this);
        gui.setUsername(AcheteurRef.getPrenom() +" "+AcheteurRef.getNom()+" ("+AcheteurRef.getLogin()+")");
        setAchats();
        gui.setVisible(true);
    }
    
    public void submitOffer(int numArticle, double prix)
    {
        EJB.makeOffer(numArticle, AcheteurRef.getId(), prix);
        gui.resetOffres();
    }
    
    public void StatusOffer(NotifyAcceptOrRefuseOfferMessage message)
    {
        if(message.getIdAcheteur() != AcheteurRef.getId())
            return; //le message n'est pas destiné à l'acheteur en cours
        
        if(message.isIsAccepted())
        {
            OffresAcceptees.add(new Proposition(message.getIdArticle(), message.getIdAcheteur(), message.getMontant(),null));
            
            DefaultTableModel dtm = new DefaultTableModel();
            
            dtm.addColumn("Numéro d'article");
            dtm.addColumn("Prix");
            Vector vect ;
            
            for(int i=0 ; i < OffresAcceptees.size() ; i++)
            {
                vect = new Vector();
                vect.add(OffresAcceptees.get(i).getArticle());
                vect.add(OffresAcceptees.get(i).getPrix());
                dtm.addRow(vect);
            }
            
            gui.setAccept(dtm);
        }
        else
        {
            OffresRefusees.add(new Proposition(message.getIdArticle(), message.getIdAcheteur(), message.getMontant(),"Refus du vendeur"));
            
            DefaultTableModel dtm = new DefaultTableModel();
            Vector vect ;


            dtm.addColumn("Numéro d'article");
            dtm.addColumn("Prix");
            dtm.addColumn("Motif de refus");

            for(int i=0 ; i < OffresRefusees.size() ; i++)
            {
                vect = new Vector();
                vect.add(OffresRefusees.get(i).getArticle());
                vect.add(OffresRefusees.get(i).getPrix());
                vect.add(OffresRefusees.get(i).getRefus());
                dtm.addRow(vect);
            }

            gui.addRefused(dtm);
        }
        
        setAchats();
    }
    
    public void StatusNotEnough(NotEnoughOfferMessage message)
    {
        if(message.getIdAcheteur() != AcheteurRef.getId())
            return; //le message n'est pas destiné à l'acheteur en cours
        
        OffresRefusees.add(new Proposition(message.getIdArticle(), message.getIdAcheteur(), message.getMontant(),"Montant en dessous de la meilleure offre"));
            
            DefaultTableModel dtm = new DefaultTableModel();
            Vector vect ;


            dtm.addColumn("Numéro d'article");
            dtm.addColumn("Prix");
            dtm.addColumn("Motif de refus");

            for(int i=0 ; i < OffresRefusees.size() ; i++)
            {
                vect = new Vector();
                vect.add(OffresRefusees.get(i).getArticle());
                vect.add(OffresRefusees.get(i).getPrix());
                vect.add(OffresRefusees.get(i).getRefus());
                dtm.addRow(vect);
            }

            gui.addRefused(dtm);
    }
    
    public void setAchats()
    {
        List<Article> articles ;
        
        articles = EJB.getArticlesForAcheteur();
        
        DefaultTableModel dtm = new DefaultTableModel();
        
        dtm.addColumn("Article");
        dtm.addColumn("Description");
        dtm.addColumn("Prix");
        dtm.addColumn("Photo");
        
        Vector vect ;
        Article article ;
        
        for(int i = 0 ; i<articles.size() ; i++)
        {
            article = articles.get(i);
            vect = new Vector();
            vect.add(article.getId());
            vect.add(article.getDescription());
            vect.add(article.getPrixAnnonce());
            if(article.getPhoto()!=null)
                vect.add(article.getPhoto());
            else
                vect.add("no picture");
            dtm.addRow(vect);
        }
        
        gui.setAchats(dtm);
    }
    
    @Override
    public void onMessage(Message message) {
        try {
            
            if(message.getIntProperty("DestinationType") != MessageConstant.TOACHETEUR)
                return;
            
            mh.updateJmsMessage(message);
            
            switch(mh.getMessageType())
            {
                case MessageConstant.NOTIFYACCEPTORREFUSEOFFERTYPE:
                    StatusOffer(mh.getNotifyAcceptOrRefuseOfferMessage());
                    break;
                case MessageConstant.NOTENOUGHOFFERTYPE:
                    StatusNotEnough(mh.getNotEnoughOfferMessage());
                    break;
            }
            
        } catch (MyMessageException | JMSException ex) {
            System.out.println(ex.getCause() + " : " + ex.getMessage());
        }  
    }
}
