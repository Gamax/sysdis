/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntitiesPackage;

import java.io.Serializable;
import java.util.Collection;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Thomas
 */
@Entity
@Table(name = "vendeur")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Vendeur.findAll", query = "SELECT v FROM Vendeur v")
    , @NamedQuery(name = "Vendeur.findById", query = "SELECT v FROM Vendeur v WHERE v.id = :id")
    , @NamedQuery(name = "Vendeur.findByNom", query = "SELECT v FROM Vendeur v WHERE v.nom = :nom")
    , @NamedQuery(name = "Vendeur.findByPrenom", query = "SELECT v FROM Vendeur v WHERE v.prenom = :prenom")
    , @NamedQuery(name = "Vendeur.findByLogin", query = "SELECT v FROM Vendeur v WHERE v.login = :login")})
public class Vendeur implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Column(name = "Nom")
    private String nom;
    @Column(name = "Prenom")
    private String prenom;
    @Column(name = "Login")
    private String login;
    @OneToMany(mappedBy = "refVendeur")
    private Collection<Lot> lotCollection;

    public Vendeur() {
        id = -1;
    }

    public Vendeur(String nom, String prenom, String login) {
        id = -1;
        this.nom = nom;
        this.prenom = prenom;
        this.login = login;
    }
    
    

    public Vendeur(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    @XmlTransient
    public Collection<Lot> getLotCollection() {
        return lotCollection;
    }

    public void setLotCollection(Collection<Lot> lotCollection) {
        this.lotCollection = lotCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Vendeur)) {
            return false;
        }
        Vendeur other = (Vendeur) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntitiesPackage.Vendeur[ id=" + id + " ]";
    }
    
}
