/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntitiesPackage;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Thomas
 */
@Entity
@Table(name = "article")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Article.findAll", query = "SELECT a FROM Article a")
    , @NamedQuery(name = "Article.findById", query = "SELECT a FROM Article a WHERE a.id = :id")
    , @NamedQuery(name = "Article.findByPrixAnnonce", query = "SELECT a FROM Article a WHERE a.prixAnnonce = :prixAnnonce")
    , @NamedQuery(name = "Article.findByPrixVente", query = "SELECT a FROM Article a WHERE a.prixVente = :prixVente")
    , @NamedQuery(name = "Article.findByVendeur", query = "SELECT a FROM Article a WHERE a.refLot.refVendeur.id = :idVendeur AND a.dateVente IS NULL")
    , @NamedQuery(name = "Article.findByLot", query = "SELECT a FROM Article a WHERE a.refLot.id = :idLot")
    , @NamedQuery(name = "Article.findUnSold", query = "SELECT a FROM Article a WHERE a.dateVente IS NULL")
    , @NamedQuery(name = "Article.findByDateVente", query = "SELECT a FROM Article a WHERE a.dateVente = :dateVente")})
public class Article implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Lob
    @Column(name = "Description")
    private String description;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PrixAnnonce")
    private BigDecimal prixAnnonce;
    @Column(name = "PrixVente")
    private BigDecimal prixVente;
    @Column(name = "DateVente")
    @Temporal(TemporalType.DATE)
    private Date dateVente;
    @Lob
    @Column(name = "PhotoPath")
    private String photoPath;
    @Lob
    @Column(name = "Photo")
    private byte[] photo;
    @JoinColumn(name = "RefAcheteur", referencedColumnName = "Id")
    @ManyToOne
    private Acheteur refAcheteur;
    @JoinColumn(name = "RefLot", referencedColumnName = "Id")
    @ManyToOne
    private Lot refLot;

    public Article() {
        id = -1;
    }

    public Article(String description, BigDecimal prixAnnonce, BigDecimal prixVente, Date dateVente, String photoPath, byte[] photo, Acheteur refAcheteur, Lot refLot) {
        
        id =-1;
        this.description = description;
        this.prixAnnonce = prixAnnonce;
        this.prixVente = prixVente;
        this.dateVente = dateVente;
        this.photoPath = photoPath;
        this.photo = photo;
        this.refAcheteur = refAcheteur;
        this.refLot = refLot;
    }
    
    

    public Article(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getPrixAnnonce() {
        return prixAnnonce;
    }

    public void setPrixAnnonce(BigDecimal prixAnnonce) {
        this.prixAnnonce = prixAnnonce;
    }

    public BigDecimal getPrixVente() {
        return prixVente;
    }

    public void setPrixVente(BigDecimal prixVente) {
        this.prixVente = prixVente;
    }

    public Date getDateVente() {
        return dateVente;
    }

    public void setDateVente(Date dateVente) {
        this.dateVente = dateVente;
    }

    public String getPhotoPath() {
        return photoPath;
    }

    public void setPhotoPath(String photoPath) {
        this.photoPath = photoPath;
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public Acheteur getRefAcheteur() {
        return refAcheteur;
    }

    public void setRefAcheteur(Acheteur refAcheteur) {
        this.refAcheteur = refAcheteur;
    }

    public Lot getRefLot() {
        return refLot;
    }

    public void setRefLot(Lot refLot) {
        this.refLot = refLot;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Article)) {
            return false;
        }
        Article other = (Article) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntitiesPackage.Article[ id=" + id + " ]";
    }
    
}
