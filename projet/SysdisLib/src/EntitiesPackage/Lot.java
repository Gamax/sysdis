/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EntitiesPackage;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Thomas
 */
@Entity
@Table(name = "lot")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Lot.findAll", query = "SELECT l FROM Lot l")
    , @NamedQuery(name = "Lot.findById", query = "SELECT l FROM Lot l WHERE l.id = :id")
    , @NamedQuery(name = "Lot.findByDateAchat", query = "SELECT l FROM Lot l WHERE l.dateAchat = :dateAchat")
    , @NamedQuery(name = "Lot.findByVendeur", query = "SELECT l FROM Lot l WHERE l.refVendeur.id = :idVendeur")
    , @NamedQuery(name = "Lot.findByPrixAchat", query = "SELECT l FROM Lot l WHERE l.prixAchat = :prixAchat")})
public class Lot implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "Id")
    private Integer id;
    @Lob
    @Column(name = "Descriptif")
    private String descriptif;
    @Column(name = "DateAchat")
    @Temporal(TemporalType.DATE)
    private Date dateAchat;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "PrixAchat")
    private BigDecimal prixAchat;
    @JoinColumn(name = "RefVendeur", referencedColumnName = "Id")
    @ManyToOne
    private Vendeur refVendeur;
    @OneToMany(mappedBy = "refLot")
    private Collection<Article> articleCollection;

    public Lot() {
        id = -1;
    }

    public Lot(String descriptif, Date dateAchat, BigDecimal prixAchat, Vendeur refVendeur) {
        
        id = -1; //todo apparement c'est mieux
        this.descriptif = descriptif;
        this.dateAchat = dateAchat;
        this.prixAchat = prixAchat;
        this.refVendeur = refVendeur;
    }
    
    

    public Lot(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescriptif() {
        return descriptif;
    }

    public void setDescriptif(String descriptif) {
        this.descriptif = descriptif;
    }

    public Date getDateAchat() {
        return dateAchat;
    }

    public void setDateAchat(Date dateAchat) {
        this.dateAchat = dateAchat;
    }

    public BigDecimal getPrixAchat() {
        return prixAchat;
    }

    public void setPrixAchat(BigDecimal prixAchat) {
        this.prixAchat = prixAchat;
    }

    public Vendeur getRefVendeur() {
        return refVendeur;
    }

    public void setRefVendeur(Vendeur refVendeur) {
        this.refVendeur = refVendeur;
    }

    @XmlTransient
    public Collection<Article> getArticleCollection() {
        return articleCollection;
    }

    public void setArticleCollection(Collection<Article> articleCollection) {
        this.articleCollection = articleCollection;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Lot)) {
            return false;
        }
        Lot other = (Lot) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "EntitiesPackage.Lot[ id=" + id + " ]";
    }
    
}
