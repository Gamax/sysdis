/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javaapplicationclienttestselfhostedws;

/**
 *
 * @author Thomas
 */
public class JavaApplicationClientTestSelfHostedWs {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        ws.MySelfHostedWs_Service service = new ws.MySelfHostedWs_Service();
        ws.MySelfHostedWs port = service.getMySelfHostedWsPort();
        System.out.println("Appel du WebService : " + port.sayHello("Toto"));
    }
    
}
