/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessTestRemote;

import businessTestLocal.SessionBeanTestLocalLocal;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 *
 * @author thoma
 */
@Stateless
public class SessionBeanTestRemote implements SessionBeanTestRemoteRemote {

    @EJB
    private SessionBeanTestLocalLocal sessionBeanTestLocal;

    @Override
    public String SayHello(String Nom) {
        return "Hello ! " + Nom + " " + sessionBeanTestLocal.GetPrenom(Nom);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
