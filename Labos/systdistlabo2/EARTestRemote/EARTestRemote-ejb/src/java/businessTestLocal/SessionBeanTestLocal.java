/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessTestLocal;

import java.util.HashMap;
import javax.ejb.Stateless;

/**
 *
 * @author thoma
 */
@Stateless
public class SessionBeanTestLocal implements SessionBeanTestLocalLocal {

    @Override
    public String GetPrenom(String Nom) {
        HashMap<String, String> map = new HashMap<String, String>();
        map.put("Dethier", "Thomas");
        map.put("Schyns","Noe");
        return map.get(Nom);
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
