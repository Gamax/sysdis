/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package applicationclienttestremote;

import businessTestRemote.SessionBeanTestRemoteRemote;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.jms.Connection;
import javax.jms.ConnectionFactory;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;
import javax.jms.Topic;

/**
 *
 * @author thoma
 */
public class Main {

    @Resource(mappedName = "jms/myTestTopic")
    private static Topic myTestTopic;

    @Resource(mappedName = "jms/myTestTopicFactory")
    private static ConnectionFactory myTestTopicFactory;

    @EJB
    private static SessionBeanTestRemoteRemote sessionBeanTestRemote;
    
    private static Connection connection = null;
    private static Session session = null;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        //System.out.println(sessionBeanTestRemote.SayHello("Dethier"));
        
        try
        {
            connection = myTestTopicFactory.createConnection();
            session = connection.createSession(false, Session.AUTO_ACKNOWLEDGE);
            connection.start();
        }
        catch(JMSException ex)
        {
            System.out.println("Main error : " + ex);
        }
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GUIClient(myTestTopic,connection,session).setVisible(true);
            }
        });
    }
    
}
