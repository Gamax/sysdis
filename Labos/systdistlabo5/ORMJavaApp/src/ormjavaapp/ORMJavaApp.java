/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ormjavaapp;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import entities.Personne;

/**
 *
 * @author thoma
 */
public class ORMJavaApp {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("ORMJavaAppPU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        try {
           Personne c = new Personne();
           c.setNom("Dethier");
           c.setPrenom("Thomas");
           
           em.persist(c);
           
           Personne p2 = em.find(Personne.class, 1);
           
            System.out.println(p2.getNom());
            
            em.getTransaction().commit();
            
        } catch (Exception e) {
            e.printStackTrace();
            em.getTransaction().rollback();
        }
        finally
        {
            em.close();
        }
        
        
    }
    
}
