/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eainjectiontestclient;

import businessInjectionTest.SessionBeanInjection1;
import businessInjectionTest.SessionBeanInjection2;
import javax.ejb.EJB;

/**
 *
 * @author thoma
 */
public class Main {

    @EJB(name = "helloejb")
    private static SessionBeanInjection1 hello;
    
    @EJB(name = "byeejb")
    private static SessionBeanInjection2 bye;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        System.out.println(sessionBeanInjectionTest.sayHello("Thomas"));
    }
    
}
