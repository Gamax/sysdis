/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessInjectionTest;

import javax.ejb.Remote;

/**
 *
 * @author thoma
 */
@Remote
public interface SessionBeanInjectionTestRemote {

    String say();
    
}
