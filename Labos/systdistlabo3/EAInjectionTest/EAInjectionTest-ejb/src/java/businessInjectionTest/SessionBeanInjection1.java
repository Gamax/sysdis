/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessInjectionTest;

import javax.ejb.Stateless;
import javax.ejb.LocalBean;

/**
 *
 * @author thoma
 */
@Stateless(name="hellobean")
public class SessionBeanInjection1 implements SessionBeanInjectionTestRemote{

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
    @Override
    public String say() {
    return "Hello";
    }
}
