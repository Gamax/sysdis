/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessInjectionTest;

import javax.ejb.Stateless;

/**
 *
 * @author thoma
 */
@Stateless
public class SessionBeanInjectionTest implements SessionBeanInjectionTestRemote {
    @Override
    public String sayHello(String name) {
        return "Hello " + name +" !!!";
    }

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")
}
