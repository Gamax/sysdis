create table personne (
  idPersonne number(11) CONSTRAINT Personne$PK primary key,
  Nom VARCHAR(45),
  Prenom VARCHAR(45)
);

drop table personne;