/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import javax.ejb.Remote;

/**
 *
 * @author thoma
 */
@Remote
public interface SessionBeanStatefulRemote {

    void Add(int nb);

    int GetTotal();
}
