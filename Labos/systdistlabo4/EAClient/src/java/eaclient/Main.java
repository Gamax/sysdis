/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eaclient;

import business.SessionBeanSingletonRemote;
import business.SessionBeanStatefulRemote;
import business.SessionBeanStatelessRemote;
import gui.GuiTestCompareSSS;
import javax.ejb.EJB;

/**
 *
 * @author thoma
 */
public class Main implements Runnable{

    @EJB
    private static SessionBeanSingletonRemote sessionBeanSingleton;

    @EJB
    private static SessionBeanStatefulRemote sessionBeanStateful;

    @EJB
    private static SessionBeanStatelessRemote sessionBeanStateless;
    
    

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        (new Thread(new Main())).start();
        
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new GuiTestCompareSSS(sessionBeanStateless, sessionBeanStateful, sessionBeanSingleton).setVisible(true);
            }
        });
    }
    
    @Override
    public void run()
    {
        while(true)
        {
            sessionBeanStateless.Add(0);
            sessionBeanStateful.Add(0);
            sessionBeanSingleton.Add(0);
        }
    }
    
}
