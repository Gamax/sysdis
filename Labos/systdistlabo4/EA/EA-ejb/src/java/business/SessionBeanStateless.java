/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package business;

import javax.ejb.Stateless;

/**
 *
 * @author thoma
 */
@Stateless
public class SessionBeanStateless implements SessionBeanStatelessRemote {
    
    private int Total =0;

    // Add business logic below. (Right-click in editor and choose
    // "Insert Code > Add Business Method")

    @Override
    public void Add(int nb) {
        Total+=nb;
    }

    @Override
    public int GetTotal() {
        return Total;
    }
    
    
}
